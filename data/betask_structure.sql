SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `organizations`;
CREATE TABLE `organizations` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `datetime_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `orders` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders` (`orders`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `organizations_rel`;
CREATE TABLE `organizations_rel` (
  `id_orgz` bigint(20) NOT NULL,
  `id_parent_orgz` bigint(20) NOT NULL DEFAULT '0',
  UNIQUE KEY `id_orgz_id_parent_orgz` (`id_orgz`,`id_parent_orgz`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

