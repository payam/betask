SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `organizations`;
CREATE TABLE `organizations` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `datetime_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `orders` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `orders` (`orders`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE `organizations`;
INSERT INTO `organizations` (`id`, `name`, `datetime_created`, `orders`) VALUES
(357131090,	'Brown Banana',	'2018-08-29 09:52:11',	8),
(419360380,	'Green Banana',	'2018-08-29 09:52:11',	9),
(1263198971,	'Yellow Banana',	'2018-08-29 09:52:11',	7),
(1322033461,	'Black Banana',	'2018-08-29 09:52:11',	10),
(1570963405,	'Phoneutria Spider',	'2018-08-29 09:52:12',	11),
(2243329829,	'Big banana tree',	'2018-08-29 09:52:11',	3),
(2590002891,	'Banana tree',	'2018-08-29 09:52:11',	2),
(3802673830,	'Paradise Island',	'2018-08-29 09:52:11',	1);

DROP TABLE IF EXISTS `organizations_rel`;
CREATE TABLE `organizations_rel` (
  `id_orgz` bigint(20) NOT NULL,
  `id_parent_orgz` bigint(20) NOT NULL DEFAULT '0',
  UNIQUE KEY `id_orgz_id_parent_orgz` (`id_orgz`,`id_parent_orgz`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `organizations_rel`;
INSERT INTO `organizations_rel` (`id_orgz`, `id_parent_orgz`) VALUES
(357131090,	2243329829),
(357131090,	2590002891),
(419360380,	2243329829),
(1263198971,	2243329829),
(1263198971,	2590002891),
(1322033461,	2243329829),
(1322033461,	2590002891),
(1570963405,	1322033461),
(2243329829,	3802673830),
(2590002891,	3802673830),
(3802673830,	0);
