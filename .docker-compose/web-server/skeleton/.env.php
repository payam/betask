<?php
/** @see \Poirot\Std\Environment\EnvBase */

define('PT_DIR_DATA', '/data/poirot');
define('PT_DIR_ROOT', '/var/www/html');
define('PT_DIR_CONFIG', constant('PT_DIR_ROOT').'/config');

define('SERVER_NAME', getenv('HOST_SERVER_NAME'));

return array(
    #'defined_const' => array(
    #'PT_DIR_DATA'          => '/data/poirot',
    #'PT_DIR_ROOT'          => '/var/www/html',

    #'PT_SERVER_URL'        => 'http://server-host/',
    #'PT_BASEURL'           => '/sub_dir/',
    #),
);