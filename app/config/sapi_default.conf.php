<?php
/**
 * Default Sapi Application Options
 */

use Poirot\Router\Interfaces\iRouterStack;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Betask\Models\Drivers\Mysql\OrganizationsRepoService;


return [

    'serviceLocator' => [
        ## Define The Concrete Implementations
        #
        'implementations' => [
            'HttpPsrRequest'  => RequestInterface::class,
            'HttpPsrResponse' => ResponseInterface::class,
            'Router'          => iRouterStack::class,

        ],

        ## Define Services
        #
        'services' => [
            'HttpPsrRequest'  => \Betask\Services\HttpRequestService::class,
            'HttpPsrResponse' => \Betask\Services\HttpResponseService::class,

            'Router'          => \Betask\Services\RouterService::class,
        ],

        'nested' => [
            'repositories' => [
                'services' => [
                    'Organizations' => [
                        OrganizationsRepoService::class,
                        'db'   => 'betask',
                        'conf' => [
                            'server' => 'betask_db-mariadb',
                            'table'  => 'organizations',
                            'user'   => getenv('MYSQL_USER'),
                            'pass'   => getenv('MYSQL_PASSWORD'),
                        ],
                    ],
                ],
            ],
        ],
    ],
];
