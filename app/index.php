<?php
use Betask\Config\ArrayLoader;
use Betask\Server\BuildServer;
use Poirot\Router\Route\RouteMethodSegment;
use Poirot\Router\Route\RouteSegment;

if (! file_exists(__DIR__ . '/vendor/autoload.php') )
    throw new \RuntimeException( "Unable to start While Components not installed.\n"
        . "- Type `composer install`; to install library dependencies.\n"
    );

if (! file_exists('/docker/initialized') )
    throw new \RuntimeException( "Initializing ... Try again after a while.\n");


## Application Consistencies and AutoLoad
#
require_once __DIR__.'/index.consist.php';


// change cwd to the application root by default
chdir(PT_DIR_ROOT);


## Build Server
#
$config = ArrayLoader::load('sapi_default');
$server = new \Betask\Server(
    new BuildServer($config)
);

## Define Routes
#
$server
    // Create Organizations Tree
    ->handleRequest(
        new RouteMethodSegment('organization.insert', [
            'criteria' => '/organizations',
            'method'   => 'POST',
        ])
        , \Betask\Actions\CreateOrganizationsAction::class // Handler
    )
    // Retrieve Nodes descendant & ancestor
    ->handleRequest(
        new RouteMethodSegment('organization.retrieve', [
            'criteria' => '/organizations/:name~(?:[^%]|%[0-9A-Fa-f]{2})+~',
            'method'   => 'GET',
        ])
        , \Betask\Actions\RetrieveOrgNodesAction::class    // Handler
    )
    // Home
    ->handleRequest(
        new RouteSegment('organization.home', [
            'criteria' => '/',
        ])
        , function () {
            die('Welcome, Organizations Server.');         // Simple Welcome Message
        }
    )
;

## Listen To Requests
#
$server->run();
