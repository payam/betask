<?php
namespace Betask\Interfaces;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


interface iRouteHandler
{
    function __invoke(RequestInterface $request, ResponseInterface $response);
}
