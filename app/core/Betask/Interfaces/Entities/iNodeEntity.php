<?php
namespace Betask\Interfaces\Entities;


interface iNodeEntity
{
    /**
     * Unique Identifier
     *
     * @return mixed
     */
    function getUID();

    /**
     * Organization Name
     *
     * @return string
     */
    function getName();

    /**
     * Parent Node
     *
     * @return mixed
     */
    function getParentUID();
}
