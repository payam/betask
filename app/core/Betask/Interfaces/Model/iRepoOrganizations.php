<?php
namespace Betask\Interfaces\Model;

use Betask\Interfaces\Entities\iNodeEntity;


interface iRepoOrganizations
{
    /**
     * Save Organization Entity
     *
     * @param iNodeEntity $entity
     *
     * @throws \Exception
     * @return iNodeEntity Include persist ID
     */
    function save(iNodeEntity $entity);


    /**
     * Get Organization Relation By Name
     *
     * @param string   $name
     * @param int|null $offset Timestamp
     * @param int      $limit
     *
     * @return \Generator([rel(string) => NodeEntity])
     */
    function getRelationsByOrgzName($name, $offset = null, $limit =100);
}
