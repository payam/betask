<?php
namespace Betask\Exceptions;


class exRouteNotMatch
    extends \RuntimeException
{
    protected $code = 404;
}
