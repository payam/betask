<?php
namespace Betask\Config;

use Poirot\Std\ErrorStack;
use Poirot\Std\Glob;
use Poirot\Std\Type\StdArray;


class ArrayLoader
{
    /**
     * Load Config Files From Given Directory
     *
     * - file name can be in form of dir/to/file then:
     *   it will looking for files
     *   "file.local.conf.php" and "file.conf.php"
     *
     * - if given argument is directory:
     *   load all files with extension
     *   ".local.conf.php" and ".conf.php"
     *
     * - then fallback into default config dir (PT_DIR_CONFIG)
     *   with basename(path) and merge loaded data
     *
     * - PT_MULTI_SITE override configs by set env name
     *
     * @param string $path file or dir path
     *
     * @return StdArray|false
     */
    static function load($path, $once = false)
    {
        $isLoaded = false;
        $config   = new StdArray;

        $globPattern = $path;
        if ( is_dir($path) ) {
            $globPattern = str_replace('\\', '/', $globPattern); // normalize path separator
            $globPattern = rtrim($globPattern, '/').'/*';
        }

        if (! is_file($path) )
            // did not given exactly name of file
            $globPattern .= '.{,local.}conf.php';

        foreach ( Glob::glob($globPattern, GLOB_BRACE) as $filePath ) {
            ErrorStack::handleException(function ($e) use ($filePath) {
                ob_end_clean();
                throw new \RuntimeException(
                    sprintf('Error while loading config: %s', $filePath)
                    , 0
                    , $e
                );
            });

            ErrorStack::handleError(E_ALL, function ($e){ throw $e; });

            ob_start();
            $fConf = include $filePath;
            if (! is_array($fConf) )
                throw new \RuntimeException(sprintf(
                    'Config file (%s) must provide array; given (%s).'
                    , $filePath
                    , \Poirot\Std\flatten($fConf)
                ));
            ob_get_clean();

            $config = $config->withMergeRecursive($fConf, false);

            ErrorStack::handleDone();
            ErrorStack::handleDone();

            $isLoaded |= true;
        }


        ## Looking in Default Config Directory
        #
        if (! $once) {
            $fallBackDirectories = [PT_DIR_CONFIG, ];

            // Check for multi-site config
            //
            if ( $siteName = getenv('PT_MULTI_SITE') )
            {
                $siteName = strtolower($siteName);

                if ( $siteName != 'false' )
                    array_push(
                        $fallBackDirectories
                        , PT_DIR_CONFIG.'/_site.'.$siteName
                    );
            }

            foreach ($fallBackDirectories as $fallBackDir)
            {
                $dirPath = dirname($path);

                if (strpos($dirPath, $fallBackDir) !== 0)
                {
                    $name = ltrim( basename($path) , '\\/' );

                    $cnf = false;
                    $stack = [$name];
                    $dirPath = realpath( str_replace($fallBackDir, '', $dirPath) );
                    $dirPath = explode(DIRECTORY_SEPARATOR, ltrim($dirPath, DIRECTORY_SEPARATOR));

                    $maxDeep = 2;
                    while (false === $cnf) {
                        if ( 0 >= $maxDeep-- )
                            break;

                        $tPath = $fallBackDir.'/'.implode('/', $stack);
                        $cnf   = self::load($tPath, true);
                        array_unshift($stack, array_pop($dirPath));
                    }

                    if ($cnf) {
                        $config = $config->withMergeRecursive($cnf, false);
                        $isLoaded |= true;
                    }
                }
            }
        }

        return ($isLoaded) ? $config : false;
    }
}
