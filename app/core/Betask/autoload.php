<?php
define('DIR_VENDOR', PT_DIR_ROOT . '/vendor');

if (! class_exists('\Poirot\Loader\Autoloader\LoaderAutoloadAggregate') )
    // Used as base skeleton; it may also installed as composer package so the required packages is available.
    require_once DIR_VENDOR. '/poirot/loader' . '/Poirot/Loader/Autoloader/LoaderAutoloadAggregate.php';


## Register Namespaces
#
$namespaces = [
    'Betask' => [ __DIR__ ],
];

$loader = new \Poirot\Loader\Autoloader\LoaderAutoloadAggregate(array(
    'Poirot\Loader\Autoloader\LoaderAutoloadNamespace' => $namespaces,
));

$loader->register(true); // true for prepend loader in autoload stack
