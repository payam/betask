<?php
namespace Betask\Models\Drivers\Mysql;

use Betask\Services\Models\Drivers\Mysql\aServiceRepoMysql;


class OrganizationsRepoService
    extends aServiceRepoMysql
{
    /**
     * Create Repo Instance
     *
     * @param \PDO $mysqlDriver
     * @param null $tableName
     *
     * @return OrganizationsRepo
     */
    function newInstance($mysqlDriver, $tableName = null)
    {
        return new OrganizationsRepo($mysqlDriver, $tableName);
    }
}
