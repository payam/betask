<?php
namespace Betask\Models\Drivers\Mysql;

use Betask\Interfaces\Entities\iNodeEntity;
use Betask\Interfaces\Model\iRepoOrganizations;
use Betask\Models\NodeEntity;
use Poirot\Std\GeneratorWrapper;
use Poirot\Std\Hydrator\HydrateGetters;


class OrganizationsRepo
    implements iRepoOrganizations
{
    /** @var \PDO */
    protected $q;
    protected $tableName;


    /**
     * Constructor.
     *
     * @param \PDO   $mysqlDriver
     * @param string $tableName
     */
    function __construct(\PDO $mysqlDriver, $tableName = 'organizations')
    {
        $this->q = $mysqlDriver;
        $this->tableName = $tableName;
    }


    /**
     * Save Organization Entity
     *
     * @param iNodeEntity $entity
     *
     * @throws \Exception
     * @return iNodeEntity Include persist ID
     */
    function save(iNodeEntity $entity)
    {
        $orgTable  = $this->tableName;
        $orgRelTab = $this->tableName.'_rel';

        $myEntity = new NodeMysqlEntity( new HydrateGetters($entity) );


        $this->q->beginTransaction();

        try {
            /// Insert Into Organizations
            //
            $sql = "REPLACE INTO {$orgTable} (id, name) VALUES (?,?)";
            $stmt= $this->q->prepare($sql);
            $stmt->execute([
                $myEntity->getUID(),
                $myEntity->getName()
            ]);


            /// Insert Relations
            //
            $sql = "REPLACE INTO {$orgRelTab} (id_orgz, id_parent_orgz) VALUES (?,?)";
            $stmt= $this->q->prepare($sql);
            $stmt->execute([
                $myEntity->getUID(),
                $myEntity->getParentUID() ?? 0 // zero means root
            ]);

            $this->q->commit();

        } catch (\Exception $e) {
            $this->q->rollBack();

            throw $e; // let exception flow
        }


        return $myEntity;
    }


    /**
     * Get Organization Relation By Name
     *
     * @param string $name
     * @param int|null $offset Timestamp
     * @param int $limit
     *
     * @return \Generator
     * @throws \Exception
     */
    function getRelationsByOrgzName($name, $offset = null, $limit =100)
    {
        $orgTable  = $this->tableName;
        $orgRelTab = $this->tableName.'_rel';


        $myEntity = new NodeMysqlEntity(['name' => $name]);
        $uid      = $myEntity->getUID();

        if ($offset !== null ) {
            if (! $entity = $this->findOneByID($offset) )
                throw new \Exception(sprintf('Invalid Offset (%s) is Given.', $offset));

            $offset = $entity->getOrders();
            $offset = "and oz.orders >= {$offset}";
        }

        $sql = <<< SQL
        (
          SELECT oz.name, oz.id, oz.datetime_created, IF(oz.id = ozr.id_parent_orgz, 'parent', 'daughter') as rel
          FROM $orgRelTab as ozr 
          INNER JOIN $orgTable as oz ON ozr.id_parent_orgz = oz.id or ozr.id_orgz = oz.id
          WHERE (ozr.id_orgz = $uid or ozr.id_parent_orgz = $uid) 
                and oz.id <> $uid $offset
          LIMIT $limit
        )
        UNION ALL
        (
          SELECT oz.name, oz.id, oz.datetime_created, "sister" as "rel" 
          FROM $orgRelTab as ozr
          INNER JOIN $orgTable as oz ON ozr.id_orgz = oz.id
          WHERE id_parent_orgz IN (
            SELECT id_parent_orgz FROM $orgRelTab
            WHERE id_orgz = $uid
          ) AND oz.id <> $uid $offset
          GROUP BY ozr.id_orgz
         LIMIT $limit
        )
        ORDER BY name
        ## Quick Fix
        LIMIT $limit 
SQL;

        $stmt = $this->q->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $result = $stmt->fetchAll();

        return new GeneratorWrapper($result, function($value)
        {
            $entity = new NodeMysqlEntity;
            $entity
                ->setName($value['name'])
                ->setTimestampCreated($value['datetime_created'])
            ;

            return [ 'rel' => $value['rel'], 'entity' => $entity ];
        });
    }


    /**
     * Find On By ID
     *
     * @param mixed $uid
     *
     * @return NodeMysqlEntity
     */
    function findOneByID($uid)
    {
        $orgTable  = $this->tableName;

        $sql = "SELECT * FROM $orgTable WHERE id = '$uid'";

        $stmt = $this->q->prepare($sql);
        $stmt->execute();
        $stmt->setFetchMode(\PDO::FETCH_ASSOC);
        $result = $stmt->fetch();

        return new NodeMysqlEntity([
            'id'   => $result['id'],
            'name' => $result['name'],
            'timestamp_created' => $result['datetime_created'],
            'orders' => $result['orders'],
        ]);
    }
}
