<?php
namespace Betask\Models\Drivers\Mysql;

use Betask\Models\NodeEntity;


class NodeMysqlEntity
    extends NodeEntity
{
    protected $orders;


    /**
     * Set UID
     * @param mixed $uid
     * @return $this
     */
    function setUID($uid)
    {
        if ($this->getUID() !== $uid)
            throw new \InvalidArgumentException(sprintf(
                'UID is not a valid identifier; given: (%s).'
                , \Poirot\Std\flatten($uid)
            ));


        $this->uid = $uid;

        return $this;
    }

    /**
     * @override Create unique identifier hash from Org. Name
     *           Here we use crc32 that gives 4billion available hash values
     *           This can be changed or not used if max data is huge
     *
     * Unique Identifier
     * @return mixed
     */
    function getUID()
    {
        if ( $this->uid )
            return $this->uid;

        return $this->uid = crc32( strtolower($this->getName()) );
    }



    /**
     * @return int
     */
    function getOrders()
    {
        return $this->orders;
    }

    /**
     * @param int $orders
     * @return $this
     */
    function setOrders($orders)
    {
        $this->orders = (int) $orders;
        return $this;
    }
}
