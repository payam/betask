<?php
namespace Betask\Models;

use Betask\Interfaces\Entities\iNodeEntity;
use Poirot\Std\ConfigurableSetter;


class NodeEntity
    extends ConfigurableSetter
    implements iNodeEntity
{
    protected $uid;
    protected $name;
    protected $parent;
    protected $timestampCreated;


    /**
     * Set UID
     * @param mixed $uid
     * @return $this
     */
    function setUID($uid)
    {
        $this->uid = $uid;
        return $this;
    }

    /**
     * Unique Identifier
     * @return mixed
     */
    function getUID()
    {
        return $this->uid;
    }

    /**
     * Set Organization Name
     * @param string $name
     * @return $this
     */
    function setName($name)
    {
        $this->name = (string) $name;
        return $this;
    }

    /**
     * Organization Name
     * @return string
     */
    function getName()
    {
        return $this->name;
    }

    /**
     * Set Parent Node UID
     * @param mixed $parent
     * @return $this
     */
    function setParentUID($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * Parent Node
     *
     * @return mixed
     */
    function getParentUID()
    {
        return $this->parent;
    }

    /**
     * Timestamp created
     *
     * @param int $timestampCreated Timestamp
     *
     * @return $this
     */
    function setTimestampCreated($timestampCreated)
    {
        $this->timestampCreated = $timestampCreated;
        return $this;
    }

    /**
     * Get Timestamp Created
     *
     * @return int Timestamp
     */
    function getTimestampCreated()
    {
        if ($this->timestampCreated)
            return $this->timestampCreated;


        return $this->timestampCreated = time();
    }
}
