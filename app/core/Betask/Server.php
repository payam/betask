<?php
namespace Betask;

use Betask\Exceptions\exRouteNotMatch;
use Betask\Server\ArgResolver;
use Betask\Server\BuildServer;
use Poirot\Ioc\Container;
use Poirot\Ioc\Interfaces\Respec\iServicesProvider;
use Poirot\Psr7\Response\PhpServer;
use Poirot\Router\Interfaces\iRoute;
use Poirot\Router\Interfaces\iRouterStack;
use Poirot\Std\ErrorStack;
use Poirot\Std\Type\StdTravers;
use Poirot\Stream\Psr\StreamBridgeInPsr;
use Poirot\Stream\Streamable;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;


class Server
    implements iServicesProvider
{
    protected const ACTION = 'action';

    /** @var Container */
    protected $ioc;


    /**
     * Server constructor.
     *
     * @param BuildServer $builder
     * @param Container   $IoC
     */
    function __construct(BuildServer $builder = null, Container $IoC = null)
    {
        // Give IoC Container
        //
        if ($IoC !== null)
            $this->ioc = $IoC;


        // Build Application with Builder Options
        //
        if ($builder !== null)
            $builder->build($this);

    }


    /**
     * Listen To Request and Execute Handlers
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    function run()
    {
        ErrorStack::handleException(function(\Throwable $e) {
            $this->_handleException($e);
        });


        ## Match Request
        #
        /** @var RequestInterface $request */
        $request  = $this->services()->get('HttpPsrRequest');
        $router  = $this->_router();

        if (! $matchRoute = $router->match($request) )
            throw new exRouteNotMatch('Route Not Match.');


        ## Attain Actions and Parameters
        #
        // route params include, Route Named Param and Executable Action.
        $params = $matchRoute->params();
        $params = StdTravers::of($params)->toArray();

        $action = $params[self::ACTION] ?? null;
        unset($params[self::ACTION]); // remove action itself from params


        ## Resolve Args. For Callable
        #
        $invokable = new ArgResolver( $this->services() );
        $invokable = $invokable->resolveActionInvokable($action, $params);

        $response  = call_user_func($invokable);


        ## Prepare Response
        #
        if ( is_array($response) ) {
            $r = json_encode($response);

            /** @var ResponseInterface $response */
            $response = $this->services()->get('HttpPsrResponse');
            $response = $response->withBody(
                new StreamBridgeInPsr(
                    new Streamable\STemporary($r)
                )
            );

            $response = $response->withHeader('Content-Type', 'application/json');
        }

        if (! $response instanceof ResponseInterface)
            throw new \Exception('Application Server Allow Only [array | response] Result From Action Callable.');


        $hContentType = $response->getHeaderLine('Content-Type');
        if ( empty($hContentType) ) {
            $response = $response->withHeader('Content-Type', 'application/json');

        } elseif ( false === strstr('application/json', $hContentType) ) {
            throw new \Exception(sprintf(
                'Application Server Response Must Be A Json; given: (%s).'
                , $hContentType
            ));
        }

        ErrorStack::handleDone();


        $this->_sendResponse($response);
    }

    /**
     * Handle Match Request
     *
     * @param iRoute          $route
     * @param callable|string $callable Maybe a class name given as callable ClassInvoke::class
     *
     * @return $this
     */
    function handleRequest(iRoute $route, $callable)
    {
        $route->params()->set(self::ACTION, $callable);
        $this->_router()->add($route);

        return $this;
    }

    /**
     * Get Sapi Name
     *
     * @return string
     */
    function getSapiName()
    {
        // apache2handler|cli| ...
        return php_sapi_name();
    }


    // iServicesProvider Implementation:

    /**
     * Services Container
     *
     * @return Container
     */
    function services()
    {
        if (! $this->ioc )
            $this->ioc = new Container;


        return $this->ioc;
    }


    // ..

    /**
     * Send Response
     *
     * @param ResponseInterface $response
     */
    function _sendResponse(ResponseInterface $response)
    {
        PhpServer::_($response)->send();
    }


    /**
     * @return iRouterStack
     */
    protected function _router()
    {
        $router = $this->services()->get('Router');
        return $router;
    }

    /**
     * @param \Throwable $e
     * @throws \Error
     * @throws \Throwable
     */
    private function _handleException($e)
    {
        throw $e;


        if ($e instanceof \Error)
            throw $e;

        $r = [
            'Status'  => 'Error',
            'Message' => $e->getMessage(),
        ];


        $r = json_encode($r);

        $code = $e->getCode();
        if ($code < 400)
            $code = 400;
        elseif ($code > 500)
            $code = 500;

        /** @var ResponseInterface $response */
        $response = $this->services()->get('HttpPsrResponse');
        $response = $response
            ->withStatus( $code )
            ->withBody(
            new StreamBridgeInPsr(
                new Streamable\STemporary($r)
            )
        );

        $response = $response->withHeader('Content-Type', 'application/json');
        $this->_sendResponse($response);
    }
}
