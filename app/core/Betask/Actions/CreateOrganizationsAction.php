<?php
namespace Betask\Actions;

use Betask\Interfaces\Entities\iNodeEntity;
use Betask\Interfaces\Model\iRepoOrganizations;
use Betask\Models\NodeEntity;
use Psr\Http\Message\RequestInterface;


class CreateOrganizationsAction
{
    /** @var RequestInterface */
    protected $request;
    /** @var iRepoOrganizations */
    protected $repoOrgz;


    /**
     * Constructor.
     *
     * @param RequestInterface   $request           @IoC /HttpPsrRequest
     * @param iRepoOrganizations $repoOrganizations @IoC /repositories/Organizations
     */
    function __construct(RequestInterface $request, iRepoOrganizations $repoOrganizations)
    {
        $this->request  = $request;
        $this->repoOrgz = $repoOrganizations;
    }


    function __invoke()
    {
        $data = $this->request->getBody();
        // If we have really large input, this is stream
        // and we can manually parse json tokens
        // for this moment we skip that.
        $body = $data->getContents();

        if (false === $jsonBody = json_decode($body, true) )
            throw new \InvalidArgumentException(
                'Invalid Request Data; Only Accept JSON.'
                , 400 // response code
            );


        ## Iterate Over Given Data and Insert To DB
        #
        $iteration = [ $jsonBody ];
        $c = $this->iterateOverNodes($iteration);


        ## Build Response
        #
        return [
            'Status'   => 'success',
            'Results'  => [
                'Messages' => sprintf('%s Organization(s) Entity Created Successfully.', $c),
                'Count'    => $c,
            ],
        ];
    }


    // ..

    /**
     * Iterate Over Nodes (Parents nodes first)
     *
     * @param array $nodes
     * @param iNodeEntity $parentUID
     *
     * @return int
     */
    protected function iterateOverNodes(array $nodes, $parentUID = null)
    {
        $daughters = [];

        $c = 0;
        foreach ($nodes as $n)
        {
            $node = new NodeEntity;
            $node
                ->setName($n['org_name']);

            if ($parentUID !== null)
                $node->setParentUID($parentUID);


            ## Persist Entity
            #
            $pEntity = $this->repoOrgz->save($node);


            if ( isset($n['daughters']) )
                $daughters[ $pEntity->getUID() ] = $n['daughters'];

            $c++;
        }

        foreach ($daughters as $parentUID => $d)
            $c += $this->iterateOverNodes($d, $parentUID);


        return $c;
    }
}
