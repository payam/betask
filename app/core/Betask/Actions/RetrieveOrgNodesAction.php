<?php
namespace Betask\Actions;

use Betask\Interfaces\Model\iRepoOrganizations;
use Betask\Models\Drivers\Mysql\OrganizationsRepo;
use Betask\Models\NodeEntity;
use Poirot\Http\Psr\ServerRequestBridgeInPsr;
use Psr\Http\Message\RequestInterface;


class RetrieveOrgNodesAction
{
    /** @var RequestInterface|ServerRequestBridgeInPsr */
    protected $request;
    /** @var iRepoOrganizations */
    protected $repoOrgz;


    /**
     * Constructor.
     *
     * @param RequestInterface   $request           @IoC /HttpPsrRequest
     * @param iRepoOrganizations $repoOrganizations @IoC /repositories/Organizations
     */
    function __construct(RequestInterface $request, iRepoOrganizations $repoOrganizations)
    {
        $this->request  = $request;
        $this->repoOrgz = $repoOrganizations;
    }


    /**
     *
     * @param string $name Organization name from route
     *
     * @return array
     */
    function __invoke($name)
    {
        $name  = rawurldecode($name);

        ## Parse Request Query params
        #
        $q      = $this->request->getQueryParams();
        $offset = (isset($q['offset'])) ? $q['offset']       : null;
        $limit  = (isset($q['limit']))  ? (int) $q['limit']  : 100;


        ## Retrieve Relations By Name
        #
        $relations = $this->repoOrgz->getRelationsByOrgzName($name, $offset, $limit+1);



        ## Prepare Response
        #
        $result = [];
        /** @var NodeEntity $entity */
        foreach ($relations as $r)
        {
            $entity = $r['entity'];
            $rel    = $r['rel'];

            $result[] = [
                'org_name' => $entity->getName(),
                'relationship_type' => $rel,
            ];
        }


        // Check whether to display fetch more link in response?
        //
        $linkMore = null;
        if (count($result) > $limit) {
            \array_pop($result);  // skip augmented content to determine has more?
            // retrieve the next from this offset (less than this)
            // Note: we use id itself here to be more compatible with other repositories engine
            // though we have to query on this id and get orders number for pagination on mysql repo
            /** @see OrganizationsRepo::getRelationsByOrgzName */
            $nextOffset = $entity->getUID();
            $linkMore   = $this->request->getUri();
            $linkMore   = (string) $linkMore->withQuery('offset='.($nextOffset).'&limit='.$limit);
        }


        ## Build Response
        #
        return [
            'Status'   => 'OK',
            'Count'    => count($result),
            'Results'  => $result,
            'LinkMore' => $linkMore,
            '_self'    => [
                'name' => $name,
            ],
        ];
    }
}
