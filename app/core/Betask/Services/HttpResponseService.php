<?php
namespace Betask\Services;

use Poirot\Http\HttpMessage\Response\BuildHttpResponse;
use Poirot\Http\HttpResponse;
use Poirot\Http\HttpMessage\Response\DataParseResponsePhp;

use Poirot\Http\Psr\ResponseBridgeInPsr;
use Poirot\Ioc\Container\Service\aServiceContainer;
use Psr\Http\Message\ResponseInterface;


class HttpResponseService
    extends aServiceContainer
{
    /** @var string Service Name */
    protected $name = 'HttpPsrResponse';


    /**
     * Create Http Response Service
     *
     * @return ResponseInterface
     */
    function newService()
    {
        $setting  = new DataParseResponsePhp;
        $response = new HttpResponse(
            new BuildHttpResponse( BuildHttpResponse::parseWith($setting) )
        );

        return new ResponseBridgeInPsr($response);
    }
}
