<?php
namespace Betask\Services;

use Betask\Services\Request\BuildHttpRequestFromPhpServer;
use Poirot\Http\HttpRequest;
use Poirot\Http\Psr\ServerRequestBridgeInPsr;
use Poirot\Ioc\Container\Service\aServiceContainer;
use Psr\Http\Message\RequestInterface;


class HttpRequestService
    extends aServiceContainer
{
    const NAME = 'HttpPsrRequest';

    /** @var string Service Name */
    protected $name = self::NAME;


    /**
     * Create Http Request Service
     *
     * @return RequestInterface
     */
    function newService()
    {
        ## build request with php sapi attributes
        $request = new HttpRequest;
        $builder = new BuildHttpRequestFromPhpServer;
        $builder->build($request);


        return new ServerRequestBridgeInPsr($request);
    }
}
