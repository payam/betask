<?php
namespace Betask\Services\Models\Drivers\Mysql;

use PDO;
use PDOException;
use Poirot\Ioc\Container\Service\aServiceContainer;


abstract class aServiceRepoMysql
    extends aServiceContainer
{
    protected $conf = [
        /*
        'db'     => 'dbname',
        'server' => 'betask_db-mariadb',
        'user'   => 'betask_root',
        'pass'   => '5hmg3vkrxgsd2swu',
        */
    ];

    static protected $conn = [
        // 'db' => $conn
    ];

    protected $dbName;



    abstract function newInstance($mysqlDriver, $tableName = null);

    /**
     * Create Service
     *
     * @return mixed
     */
    final function newService()
    {
        $db = $this->dbName;

        if (! isset(static::$conn[$db])) {

            try {
                $cn = "mysql:host={$this->conf['server']};dbname={$db}";

                if ($this->conf['port'])
                    $cn .= ";port=".$this->conf['port'];

                $conn = new PDO($cn
                    , $this->conf['user']
                    , $this->conf['pass']
                );
                // set the PDO error mode to exception
                $conn->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES 'utf8'");
                $conn->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET CHARACTER SET 'utf8'");
                $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $conn->exec("SET NAMES utf8");
                #$conn->exec("SET CHARACTER SET utf8");
                static::$conn[$db] = $conn;
            }
            catch(PDOException $e)
            {
                throw $e;
            }
        }

        return $this->newInstance(static::$conn[$db], $this->conf['table'] ?? null);
    }


    // Options:

    function setConf(array $conf)
    {
        $this->conf = $conf;
    }

    function setDb($dbName)
    {
        $this->dbName = $dbName;
    }
}
