<?php
namespace Betask\Server;

use Poirot\Ioc\Container;
use Poirot\Ioc\instance;
use Poirot\Ioc\Interfaces\iContainer;
use Poirot\Ioc\Interfaces\Respec\iServicesProvider;


class ArgResolver
    implements iServicesProvider
{
    /** @var Container */
    protected $services;


    /**
     * ArgResolver constructor.
     *
     * @param Container $ioc
     */
    function __construct(Container $ioc)
    {
        $this->services = $ioc;
    }


    /**
     * Invoke Callable Action
     *
     * @param callable $action
     * @param array    $params
     *
     * @return callable
     */
    function resolveActionInvokable(/*callable*/$action, $params)
    {
        if (! is_callable($action) ) {
            if (is_string($action))
                $action = $this->_resolveStringToCallable($action, $params);
        }

        if (! is_callable($action) )
            throw new \RuntimeException(sprintf(
                'Action Must Be Callable; given: (%s).', \Poirot\Std\flatten($action)
            ));


        ## get required services from module::initServicesWhenModulesLoaded
        $requiredParams = [];
        $reflectParams = \Poirot\Std\Invokable\reflectCallable($action)->getParameters();
        foreach($reflectParams as $reflectionParam)
            // ['router', ...]
            $requiredParams[] = $reflectionParam->getName();

        // ['router' => iHRouter] attain service object from name
        $availableArgs = $this->_attainRequestedServicesFromContainer(
            $this->services()
            , $requiredParams
        );

        // route params is on higher priority that services if given
        $availableArgs = array_merge($availableArgs, $params);

        try {
            $reflection       = \Poirot\Std\Invokable\reflectCallable($action);
            $matchedArguments = \Poirot\Std\Invokable\resolveArgsForReflection($reflection, $availableArgs);
        } catch (\Exception $e ) {
            throw new \RuntimeException(sprintf(
                'The Arguments (%s) cant resolved neither with params or available arguments for action (%s).'
                , implode(', ', $reflectParams), get_class($action)
            ));
        }

        if (array_intersect_key($matchedArguments, $availableArgs) === $matchedArguments) {
            ## invoke method with resolved arguments
            ## all arguments is resolved from ioc container and given parameters
            return $action = function() use ($action, $matchedArguments) {
                return call_user_func_array($action, $matchedArguments);
            };
        }
        ## else:
        ## It has arguments that must resolve from previous action chains and default params
        ## give current given options to action and make runtime function with arguments that not resolved

        // build function arguments "$identifier = null, $flag = false"
        if ($matchedArguments === null)
            $matchedArguments = $requiredParams;

        $args = []; $replacement = '';
        $d = array_diff_key($matchedArguments, $availableArgs);
        foreach ($d as $k => $v) {
            $v    = var_export($v ,true);
            $args[$k] = "\${$k} = {$v}";
            // Add TypeHint So Let Resolver To Resolve By TypeHint
            /** @var \ReflectionParameter $rp */
            foreach ($reflectParams as $i => $rp) {
                $name = $rp->getName();
                if ($name !== $k)
                    continue;

                $typeHint = null;
                if (method_exists($rp, 'getType')) {
                    // PHP7
                    $typeHint = $rp->getType();
                    $args[$name] = $typeHint.' '.$args[$name];
                }

                unset($reflectParams[$i]);
            }

            // build argument replacement "$matchedArguments['identifier'] = $identifier;"
            $replacement .= "\$matchedArguments['{$k}'] = \${$k};";
        }



        $args = implode(', ', $args);
        $evalFunc = "return function({$args}) use (\$action, \$matchedArguments) {
            $replacement
            return call_user_func_array(\$action, \$matchedArguments);
        };";

        $action = eval($evalFunc);
        return $action;
    }

    /**
     * @param Container $services
     * @param array     $requiredServices
     * @return array
     */
    protected function _attainRequestedServicesFromContainer($services, $requiredServices)
    {
        $params = array();
        foreach($requiredServices as $serviceName) {
            if ($serviceName == 'services')
                ## container self as "services" name
                $service = $services;
            else {
                if (!$services->has($serviceName))
                    continue;

                $service = $services->get($serviceName);
            }

            $params[$serviceName] = $service;
        }

        return $params;
    }

    /**
     * Resolve to aResponder
     *
     * - action name from nested containers:
     *   '/module/application/action/view_page'
     *   from module->application.action, get view_page action
     *
     * @param string    $aResponder
     *
     * @return callable
     */
    protected function _resolveStringToCallable($aResponder, $params)
    {
        /** @see ListenerInitNestedContainer */
        $services   = $this->services();

        if ( class_exists($aResponder) )
            return $aResponder = \Poirot\Ioc\newInitIns(
                new instance($aResponder, $params)
                , $services
            );


        ## get action from service container
        #
        try {
            $aResponder = $services->get( $aResponder, $params );
        } catch (\Exception $e) {
            throw new \RuntimeException(
                sprintf('Dispatcher cant resolve to (%s).', $aResponder), 500, $e
            );
        }

        return $aResponder;
    }


    // Implement Service Locator:

    /**
     * Services Container
     *
     * @return iContainer
     */
    function services()
    {
        return $this->services;
    }
}
