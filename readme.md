# Pipedrive Backend Engineer Task Application
This is test application for a Backend programmer job in Pipedrive.


## How to run on Docker
You need Docker and Docker-Compose Installed On Your Device.
 
## Up Containers
a) make a copy from ```docker-compose.local.yml.dist``` to ```docker-compose.local.yml``` 
 
```
cp docker-compose.local.yml.dist docker-compose.local.yml.dist
```

ab) (optional) edit content of docker-compose.local.yml
```
betask_web-server:
    ## --> Maybe you want to change the running port number
    ports:
      - "8000:80"
    environment:
      ## --> Maybe Turn Debug mode Off; by setting to False
      ## XDebug:
      - DEBUG=True
      ## --> Your Device IP Address Here 
      # when debug is true required
      - HOST_IP=192.168.1.51
```
 
b) Up Containers
```
docker-compose -f docker-compose.yml -f docker-compose.local.yml up -d
```
 
c) Import DB Structure Or Sample Data
  First Accessing Onto The Container By Executing Bash Command 
```
docker-compose exec betask_db-mariadb /bin/bash
```
 
Then inside the container Import Data:
```
mysql -u betask_root root -p5hmg3vkrxgsd2swu betask < betask_structure.sql
```
 
Or For Sample Data:
```
mysql -u betask_root root -p5hmg3vkrxgsd2swu betask < betask_sample_data.sql
```


### To See What happens inside containers (logs)

```
docker-compose logs -f --tail=100
```

#### That's it! Thank You.